const HDWalletProvider = require('@truffle/hdwallet-provider');
require("dotenv").config();
const INFURA_API_KEY = process.env.INFURA_API_KEY;
const MNEMONIC = process.env.MNEMONIC;
const ETHERSCAN_API_KEY = process.env.ETHERSCAN_API_KEY;
const POLYGONSCAN_API_KEY = process.env.POLYGONSCAN_API_KEY;

module.exports = {
  
  networks: {

    test: {
     host: "127.0.0.1",     
     port: 8545,           
     network_id: "*",       
    },

    goerli: {
      provider: () => {
        return new HDWalletProvider(MNEMONIC, 'wss://goerli.infura.io/ws/v3/' + INFURA_API_KEY)
      },
      network_id: 5, 
      timeoutBlocks: 200,
      skipDryRun: true,
    },
    mumbai: {
            provider: () =>
              new HDWalletProvider(
                MNEMONIC,
                `https://matic-mumbai.chainstacklabs.com/`
              ),
            network_id: 80001,
            timeoutBlocks: 200,
            skipDryRun: true,
            networkCheckTimeout: 1000000
          },
  },
  compilers: {
    solc: {
      version: "0.8.7",   
    }
  },
  plugins: [
    "solidity-coverage",
    "truffle-plugin-verify"
  ],
  api_keys: {
    etherscan: ETHERSCAN_API_KEY,
    polygonscan: POLYGONSCAN_API_KEY
  },
};
