// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.7;


interface IERC721Token{
    /** 
     * @dev Event emitted when nft will airdrop
    */
    event AirdroppedNFT(uint256 tokenId);

    /** 
     * @dev Event emitted when nft is minted
    */
    event NewNFTMinted(uint256 tokenId);

    /** 
     * @dev Event emitted when token price is updated
    */
    event updatedNewTokenPrice(uint256 _tokenPrice);

    /**
     * @dev Event emitted when owner withdraw contract balance
    */
    event withdrawnEthers(address _to);

    /**
     * @dev Mint a new token 
    */
    function safeMint(address to, string memory tokenUri) external payable;

    /**
     * @dev Airdrop the Tokens to assigned address by the owner
    */
    function airDrop(address[] calldata account, string[] memory tokenUri) external;

    /**
     * @dev balanceOfContract return the balance of contract
    */
    function balanceOfContract() external view returns(uint256);

    /**
     * @dev Withdraw ethers which pay during the mint 
    */
    function withdraw() external;

    /**
     * @dev Update the Token Price 
    */
    function updateTokenPrice(uint256 _mintPrice) external;

    /**
     * @dev Added the account to blacklist
     */
    function addToBlackList(address[] memory _user) external returns (bool);

    /**
     * @dev Removes the account from blacklist
     */
    function removeFromBlackList(address[] memory _user) external returns (bool);

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
    */
    function pause() external;

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function unpause() external;
}
