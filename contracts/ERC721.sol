// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

/**
  *  @title ERC721Token contract for creating ERC721 standard (Non-Fungible) Token
  *  @author The Systango Blockchain Team
*/ 

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

import "./IERC721.sol";
import "./BlackListUser.sol";

contract ERC721Token is ERC721, Pausable, Ownable, IERC721Token, BlackList {

    using Counters for Counters.Counter;

    /**
     * @dev Counter for maintaining TokenIDs
    */
    Counters.Counter public _tokenIdCounter;

    /**
     *  @dev Get Zero Address
    */
    address constant ZERO_ADDRESS = address(0);

    /**
     *  @dev ERC721 Token Price for TokenId
    */
    uint256 public tokenPrice;

    /**
     *  @dev Mapping for maintaing URIs for each NFT token
    */
    mapping(uint256 => string) public uri;

    /**
     *  @dev Initialize token Name
     *  @dev Initialize Token Symbol
     *  @dev Initialize Token Price
    */
    constructor(string memory _name,string memory _symbol, uint256 _tokenPrice)  ERC721(_name,_symbol) { 
        tokenPrice = _tokenPrice;
    }

    /**
     * @notice tokenURI, function is used to check uri of specific token by giving tokenId.
     * @dev tokenURI, function getting the token uri by given tokenId.
     * @param id, tokenId is given to the function to get corresponding uri.
     * @return tokenURI, function return token uri.
    */
    function tokenURI(uint id) public view  override returns (string memory) {
        require(_exists(id), "ERC721Token: Non-existent token.");
        return uri[id];
    }

    /**
     * @notice user can mint token by paying token amount.
     * @dev safeMint, function is used to mint token.
     * @param to, it is account address where you want to mint.
     * @param tokenUri, tokenUri is set corresponding to the mint tokenId.
    */
    function safeMint(address to, string memory tokenUri) external payable override whenNotPaused whenNotBlackListedUser(to) {
        require(to != ZERO_ADDRESS , "ERC721Token: Address Cannot be Zero Address");
        require(msg.value >= tokenPrice, "ERC721Token: Not enough ether sent");
        require(!_isBlackListUser(_msgSender()), "ERC721Token: Caller address is blacklisted");
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        uri[tokenId] = tokenUri;
        _safeMint(to, tokenId);
        emit NewNFTMinted(tokenId);
    }

    /**
     * @notice only owner can transfer token to the specific account. 
     * @dev owner can mint token to the account.
     * @dev only owner can mint.  
     * @param account, account address where owner wants to mint token.
     * @param tokenUri, tokenUri is set corresponding to the mint tokenId.
    */
    function airDrop(address[] calldata account, string[] memory tokenUri) external override whenNotPaused onlyOwner {
        require(account.length == tokenUri.length, "ERC721Token: Incorrect parameter length");
        for(uint16 i = 0; i < account.length; i++){
            require(address(account[i]) != ZERO_ADDRESS , "ERC721Token: Address Cannot be Zero Address");
            uint256 tokenId = _tokenIdCounter.current();
            _tokenIdCounter.increment();
            uri[tokenId] = tokenUri[i];
            _mint(account[i], tokenId);
            emit AirdroppedNFT(tokenId);                
        }
    }

    /**
     * @notice balanceOfContract is used to check contract balance.
     * @dev balanceOfContract Returns the contract balance.
     * @return , it return Balance Of Contract balance in wei.
    */
    function balanceOfContract() public override view returns(uint256) {
       return address(this).balance;
    }

    /**
     * @notice withdraw is used to withdraw your token amount which you paid during minting.
     * @dev transfer token amount to contract.
    */
    function withdraw() external override whenNotPaused onlyOwner {
        require(balanceOfContract() > 0, "ERC721Token: Insufficient balance");
        address payable _to = payable(msg.sender);
        bool sent = _to.send(balanceOfContract());
        require(sent, "Failed to send Ether");
        emit withdrawnEthers(_to);
        // payable(owner()).transfer(balanceOfContract());

    }

    /**
     * @notice updateTokenPrice is use to update the token price.
     * @dev set the new token price.
     * @dev only owner can set the new token price.
     * @param _tokenPrice is updated token price in wei.
    */
    function updateTokenPrice(uint256 _tokenPrice) public override onlyOwner{
        require(_tokenPrice != tokenPrice,"ERC721Token: Current token price should be greater then existing");
        tokenPrice = _tokenPrice;
        emit updatedNewTokenPrice(_tokenPrice);
    }

    /**
      *  @notice addToBlackList function is used to add user in blacklist, now user can't mint.
      *  @dev This function would add an address to the blacklist mapping.
      *  @dev Only the owner can call this function.
      *  @param user The account to be added to blacklist.
    */
    function addToBlackList(address[] memory user) external override onlyOwner whenNotPaused returns (bool) {
        for (uint256 index = 0; index < user.length; index++) {
            if( user[index] != ZERO_ADDRESS && !_isBlackListUser(user[index])){
                _addToBlackList(user[index]);
            }
        }
        return true;
    }

    /**
      *  @notice removeFromBlackList function is used to remove user from blacklist, now user can mint.
      *  @dev This function would remove an address from the blacklist mapping.
      *  @dev Only the owner can call this function.
      *  @param user The account to be removed from blacklist.
    */
    function removeFromBlackList(address[] memory user) external override onlyOwner whenNotPaused returns (bool) {
        for (uint256 index = 0; index < user.length; index++) {
            if( user[index] != ZERO_ADDRESS && _isBlackListUser(user[index])){
                _removeFromBlackList(user[index]);
            }
        }
        return true;
    }

    /**
     * @dev pause function is used to pause the contract
     * @dev only owner can pause the contract
    */
    function pause() public override onlyOwner {
        _pause();
    }

    /**
     * @dev unpause function is used to unpause the contract
     * @dev only owner can unpause the contract
    */
    function unpause() public override onlyOwner {
        _unpause();
    }
}
