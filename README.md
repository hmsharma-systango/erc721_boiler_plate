# The ERC721 Token BoilerPlate For Creating Your Own Token


## Getting started

To make it easy for you to make your own ERC721 standard Token.


## Table of Contents ##
1. [Setup](#Setup)
2. [Commands](#Commands)
3. [Contract Compile](#Contract-Compile)
4. [Truffle Config file](#Truffle-Config-File)
5. [Deploy On Local Network](#Deploy-On-Local-Network)
6. [Deploy On Testnet Network](#Deploy-On-Testnet-Network)
7. [Test Case Coverage](#Test-Case-Coverage)


## Setup

1. System Setup 

You'll need node, npm and the Truffle Suite to set up the development environment. 

- [Node](https://nodejs.org/en/)
    - Version - v16.13.2 or above
- [Truffle Suite](https://www.trufflesuite.com/)
    - Version - v5.5.1 or above

- [Ganache](https://www.npmjs.com/package/ganache)
    - Version - v7.5.0 or above

2. Wallet Setup

You will need a Blockchain wallet (Metamask) to deploy and test these smart contracts.

`Metamask` : -
          MetaMask is a popular cryptocurrency wallet known for its ease of use, availability on both desktops and mobile devices, the ability to buy, send, and receive cryptocurrency from within the wallet, and collect non-fungible tokens (NFTs) across two blockchains.

- To create a wallet on metamask install the Metamask extension on your web browser.
- Click on the Metamask extension and select Create a Wallet option from there.
- Setup a password for your Metamask login (Remember this is your Metamask login password not the account password).
- Tap to reveal the Secret Recovery Phrase and keep it safe with you.
- Confirm your Secret Recovery Phrase saved with you to add your account to Metamask.
- Now you can switch between Ethereum mainnet and other Test Networks.

3. .env 

```cmd
NAME=<Place your ERC721 Token Name here>
SYMBOL=<Place your ERC721 Token Symbol here>
TOKENPRICE=<Place your ERC721 Token Price here>
INFURA_API_KEY=<Place your Infura API key here for contract deployment>
MNEMONIC=<Place your metamask wallet MNEMONIC key here for contract deployment>
ETHERSCAN_API_KEY=<Place your ETHERSCAN_API_KEY here for contract verify on Etherscan Network>
POLYGONSCAN_API_KEY=<Place your POLYGONSCAN_API_KEY here for contract verify on POLYGON Network>
```
- NOTE-1:- SignUp/Login on Infura to get [INFURA_API_KEY](https://blog.infura.io/post/getting-started-with-infura-28e41844cc89)

- NOTE-2 :- Add metask extension on your browser to get [MNEMONIC](https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en)

- NOTE-3:- SignUp/Login on Etherscan to get [ETHERSCAN_API_KEY](https://etherscan.io/register)

- NOTE-4:- SignUp/Login on Polygon to get [POLYGONSCAN_API_KEY](https://polygonscan.com/register)

- Note-5:- You need some test ether to deploy the contract on test network [GET_TEST_ETHERS](https://goerlifaucet.com/) OR you can also use other faucet 

## Commands


  Initially Required Dependencies

1. Truffle Installation command

- `Truffle` : -
          Truffle is a world-class development environment, testing framework and asset pipeline for blockchains using the Ethereum Virtual Machine (EVM), aiming to make life as a developer easier.

```console
npm install -g truffle
``` 
2. Ganache Installation command

-  `Ganache` : -
          Ganache is a private Ethereum blockchain environment that allows to you emulate the Ethereum blockchain so that you can interact with smart contracts in your own private blockchain.

```console
npm install -g ganache
```
3. For Rest Required Dependenices command
```console
npm install
``` 
    
  Dependencies List
  - @openzeppelin/test-helpers
  - @truffle/hdwallet-provider
  - dotenv
  - solidity-coverage
  - truffle
  - truffle-plugin-verify


## Contract Compile

  ```console
    truffle compile --all
  ```


## Truffle Config File

This file would configure your project 

```js
const HDWalletProvider = require('@truffle/hdwallet-provider');
require("dotenv").config();
const INFURA_API_KEY = process.env.INFURA_API_KEY;
const MNEMONIC = process.env.MNEMONIC;
const ETHERSCAN_API_KEY = process.env.ETHERSCAN_API_KEY;
const POLYGONSCAN_API_KEY = process.env.POLYGONSCAN_API_KEY;

module.exports = {
  
  networks: {

    test: {
     host: "127.0.0.1",     
     port: 8545,           
     network_id: "*",       
    },

    goerli: {
      provider: () => {
        return new HDWalletProvider(MNEMONIC, 'wss://goerli.infura.io/ws/v3/' + INFURA_API_KEY)
      },
      network_id: 5, 
      timeoutBlocks: 200,
      skipDryRun: true,
    },
    mumbai: {
            provider: () =>
              new HDWalletProvider(
                MNEMONIC,
                `https://matic-mumbai.chainstacklabs.com/`
              ),
            network_id: 80001,
            timeoutBlocks: 200,
            skipDryRun: true,
          },
  },
  compilers: {
    solc: {
      version: "0.8.7",   
    }
  },
  plugins: [
    "solidity-coverage",
    "truffle-plugin-verify"
  ],
  api_keys: {
    etherscan: ETHERSCAN_API_KEY
  },
};
```
## Deploy On Local Network


Network Name - test

- To run smart contract on test first start

    `ganache-cli -d`

    in another terminal

- To migrate the contracts 

    `truffle migrate --reset --network test`

    - This will use the migrations/2_deploy_contract.js file and deploy the ERC721 contract.

        This file would use your NAME,SYMBOL and TOKENPRICE fields from .env file and pass to the smart contract.

- To test the contracts 

    `truffle test --network test`

    - This will use the test/ERC721TokenTestCase.js file and test the ERC721 contract.

## Deploy On Goerli Test Network 


Network Name - goerli

- To migrate the contracts 

    `truffle migrate --reset --network goerli`

    - This will use the migrations/2_deploy_contract.js file and deploy the ERC721 contract.

        This file would use your environment fields from .env file and pass to the smart contract.
        Before deploying the contract to Mainnet make sure you have tested everything on local and corrected, as deployment on Mainnet will involve real coins and gas fees.

## Deploy On Mumbai(Polygon) Test Network 


Network Name - mumbai

- To migrate the contracts 

    `truffle migrate --reset --network mumbai`

    - This will use the migrations/2_deploy_contract.js file and deploy the ERC721 contract.

        This file would use your environment fields from .env file and pass to the smart contract.
        Before deploying the contract to Mainnet make sure you have tested everything on local and corrected, as deployment on Mainnet will involve real coins and gas fees.

` Note:- You can deploy your Smart Contract either goerli or mumbai it's your choice (Both are test network).`


## Test Case Coverage

To run the unit test case coverage on the smart contract we have used solidity-coverage npm package. The command to run the test coverage is:

- `truffle run coverage` 


File                |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
--------------------|----------|----------|----------|----------|----------------|
 contracts/         |      100 |     87.5 |      100 |      100 |                |
  BlackListUser.sol |      100 |      100 |      100 |      100 |                |
  ERC721.sol        |      100 |    86.36 |      100 |      100 |                |
  IERC721.sol       |      100 |      100 |      100 |      100 |                |
--------------------|----------|----------|----------|----------|----------------|
All files           |      100 |     87.5 |      100 |      100 |                |

