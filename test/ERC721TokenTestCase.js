const {
    BN, // Big Number support
    constants, // Common constants, like the zero address and largest integers
    expectEvent, // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
} = require("@openzeppelin/test-helpers");
const ether = require("@openzeppelin/test-helpers/src/ether");
const { expect, use } = require("chai");
const { ZERO_ADDRESS } = require("@openzeppelin/test-helpers/src/constants");

const { fromWei, toWei } = require("web3-utils");


const ERC721Token = artifacts.require("ERC721Token");


contract("ERC721Token", (accounts) => {
    const EventNames = {
        Paused:"Paused",
        Unpaused:"Unpaused",
        AirdroppedNFT: "AirdroppedNFT",
        NewNFTMinted: "NewNFTMinted",
        updatedNewTokenPrice: "updatedNewTokenPrice",
        AddedToBlackList: "AddedToBlackList",
        RemovedFromBlackList: "RemovedFromBlackList"
    }; 

    const baseURI = "http://google.co.in/";
    const baseURI2 = "http://facebook.co.in/"
    const baseURI3 = "http://yahoo.co.in/"
    const name = 'MyToken';
    const symbol = 'MKT'
    let tokenPrice = 1000;
    const updatedTokenPrice = 1200;

    let newTokenPrice = 10000;
    let mintPrice = 100;
    const mintAmount = 1;
    let total = 0;
    let tokenIds = 0;
    let tokenNotExist = 10
    let ERC721TokenInstance = null;
    
    const [owner, user, bob,honey,george,harry] = accounts;

    async function initContract() {
        ERC721TokenInstance = await ERC721Token.new(name, symbol, tokenPrice, { from: owner });
    }

    before("Deploy ERC721Token Contract", async () => {
        await initContract();
    });

    describe("Initial State", () => {
        describe("when ERC721Token contract is instantiated", function () {
            it("has a name", async function () {
                expect(await ERC721TokenInstance.name()).to.equal(name);
            });
            it("has a symbol", async function () {
                expect(await ERC721TokenInstance.symbol()).to.equal(symbol);
            });
            it("has a tokenPrice", async function () {
                expect((await ERC721TokenInstance.tokenPrice()).toString()).to.equal(tokenPrice.toString());
            });
            it("has a admin", async function () {
                expect(await ERC721TokenInstance.owner()).to.equal(owner);
            });
            it("should create a new  contract address", async () => {
                expect(ERC721TokenInstance.address);
            });
        });
    });
    describe("safeMint", () =>{
        describe("when blacklist user try to mint token", async () => {
            it("should not mint", async () => {
                expect((await ERC721TokenInstance.addToBlackList([george])));
                await expectRevert(
                    ERC721TokenInstance.safeMint(george,baseURI2,{
                        from: george,
                        value: newTokenPrice
                    }),
                    "ERC721Token: This address is in blacklist"
                );
            });
        });
        describe("when blacklist caller try to mint token", async () => {
            it("should not mint token for user", async () => {
                expect((await ERC721TokenInstance.addToBlackList([user])));
                await expectRevert(
                    ERC721TokenInstance.safeMint(honey,baseURI2,{
                        from: user,
                        value: newTokenPrice
                    }),
                    "ERC721Token: Caller address is blacklisted"
                );
            });
        });
        describe("when zero index address try to mint", async () => {
            it("should not mint zero address    ", async () => {
                await expectRevert(
                    ERC721TokenInstance.safeMint(constants.ZERO_ADDRESS,baseURI2,{
                        from: owner,
                        value: tokenPrice
                    }),
                    "ERC721Token: Address Cannot be Zero Address"
                );
            });
        });
        describe("when user want to mint with insufficient token", async () => {
            it("should not mint tokens for insufficient balance tokens", async () => {
                await expectRevert(
                    ERC721TokenInstance.safeMint(bob,baseURI2,{
                        from: bob,
                        value: mintPrice
                    }),
                    "ERC721Token: Not enough ether sent"
                );
            });
        });
        describe("when valid user want to mint with valid token price", async () => {
            it("should mint new token", async () => {
                const mintReciept = await ERC721TokenInstance.safeMint(honey,baseURI2,{
                    from: honey,
                    value: newTokenPrice
                });
                await expectEvent(mintReciept, EventNames.NewNFTMinted, {
                    tokenId: new BN(total),
                });
                total = total + mintAmount;
                expect((await ERC721TokenInstance._tokenIdCounter()).toString()).to.equal(total.toString());
        
            });
        });
    });
    describe("airDrop", async () => {
        describe("when other user tries to airDrop token", async () => {
            it("should not airDrop token", async () => {
                await expectRevert(
                    ERC721TokenInstance.airDrop([bob,honey],[baseURI2, baseURI3],{
                        from: user,
                    }),
                    "Ownable: caller is not the owner"
                );
            });
        });
        describe("when zero index address try to mint", async () => {
            it("should not mint zero address    ", async () => {
                await expectRevert(
                    ERC721TokenInstance.airDrop([constants.ZERO_ADDRESS],[baseURI3],{
                        from: owner,
                    }),
                    "ERC721Token: Address Cannot be Zero Address"
                );
            });
        });
        describe("when owner tries to airDrop token", async () => {
            it("should not airDrop token for incorrect length", async () => {
                await expectRevert(
                    ERC721TokenInstance.airDrop([bob],[baseURI2, baseURI2],{
                        from: owner,
                    }),
                    "ERC721Token: Incorrect parameter length"
                );
            });
            it("should airDrop token", async () => {

                const airDropReciept = await ERC721TokenInstance.airDrop([bob, harry],[baseURI2, baseURI3],{
                    from: owner,
                });
                await expectEvent(airDropReciept, EventNames.AirdroppedNFT, {
                    tokenId: new BN(total),
                });
                total = total + 2;
                expect((await ERC721TokenInstance._tokenIdCounter()).toString()).to.equal(total.toString());

            });
        });
    });
    describe("balanceOfContract", () => {
        it("should get balance of contract", async () => {
            let contracBalance = await ERC721TokenInstance.balanceOfContract();
            expect(Number(contracBalance)).to.equal(Number(newTokenPrice));
        });
    });
    describe("withdraw", () =>{
        describe("when other user tries to withdraw token amount", async () => {
            it("should not withdraw token", async () => {
                await expectRevert(
                    ERC721TokenInstance.withdraw({
                        from: user,
                    }),
                    "Ownable: caller is not the owner"
                );
            });
        });
        describe("when owner tries to withdraw contract balance", async () => {
            it("should withdraw token", async () => {
                await expect(
                    ERC721TokenInstance.withdraw({
                        from: owner,
                    }),
                );
            });
        });   
        describe("when owner tries to withdraw for insufficient contract balance", async () => {
            it("should not withdraw token", async () => {
                await expectRevert(
                    ERC721TokenInstance.withdraw({
                        from: owner,
                    }),
                    "ERC721Token: Insufficient balance"
                );
            });
        });
        
    });
    describe("updateTokenPrice", () =>{
        describe("when other user tries to update token price", async () => {
            it("should not update token price", async () => {
                await expectRevert(
                    ERC721TokenInstance.updateTokenPrice(updatedTokenPrice,{
                        from: user,
                    }),
                    "Ownable: caller is not the owner"
                );
            });
        });
        describe("when owner tries to update token price", async () => {
            it("should update token price", async () => {
                const mintReciept = await ERC721TokenInstance.updateTokenPrice(updatedTokenPrice,{
                    from: owner,
                });
                await expectEvent(mintReciept, EventNames.updatedNewTokenPrice, {
                    _tokenPrice: new BN(updatedTokenPrice),
                });
                expect((await ERC721TokenInstance.tokenPrice()).toString()).to.equal(updatedTokenPrice.toString());
            });
        });
        describe("when owner tries to update token price with existing token price", async () => {
            it("should not update token price", async () => {
                await expectRevert(
                    ERC721TokenInstance.updateTokenPrice(updatedTokenPrice,{
                        from: owner,
                    }),
                    "ERC721Token: Current token price should be greater then existing"
                );
            });
        });
    });
    describe("addToBlackList", () =>{
        describe("when other user tries to add blocklist", async () => {
            it("should not add in blocklist price", async () => {
                await expectRevert(
                    ERC721TokenInstance.addToBlackList([bob],{
                        from: user,
                    }),
                    "Ownable: caller is not the owner"
                );
            });
        });
        describe("when owner tries to add user in blacklist", async () => {
            it("should added in blacklist", async () => {
                const blackListUserReceipt = await ERC721TokenInstance.addToBlackList([bob],{
                    from: owner,
                });
                await expectEvent(blackListUserReceipt, EventNames.AddedToBlackList, {
                    _user: bob,
                });
            });
        });
         
    });
    describe("removeFromBlackList", () =>{
        describe("when other user tries to add blocklist", async () => {
            it("should not add in blocklist price", async () => {
                await expectRevert(
                    ERC721TokenInstance.removeFromBlackList([bob],{
                        from: user,
                    }),
                    "Ownable: caller is not the owner"
                );
            });
        });
        describe("when owner tries to remove from blacklist", async () => {
            it("should remove from blacklist", async () => {
                const blackListUserReceipt = await ERC721TokenInstance.removeFromBlackList([bob],{
                    from: owner,
                });
                await expectEvent(blackListUserReceipt, EventNames.RemovedFromBlackList, {
                    _user: bob,
                });

          });
        });
         
     });

    describe("pause", () => {
        describe("when other user tries to pause contract", function () {
            it("should not pause contract", async () => {
                expect(await ERC721TokenInstance.paused()).to.equal(false);
                await expectRevert(
                    ERC721TokenInstance.pause({
                        from: user,
                    }),
                    "Ownable: caller is not the owner"
                );
            });
        });

        describe("when owner tries to pause contract", function () {
            it("should pause contract", async () => {
                expect(await ERC721TokenInstance.paused()).to.equal(false);
                const pauseReceipt = await ERC721TokenInstance.pause({
                    from: owner,
                });
                await expectEvent(pauseReceipt, EventNames.Paused, {
                    account: owner,
                });
                expect(await ERC721TokenInstance.paused()).to.equal(true);
            });
        });

        describe("When contract is pause", async () => {
            it("should not mint token", async function () {
                expect(await ERC721TokenInstance.paused()).to.equal(true);
                await expectRevert(
                    ERC721TokenInstance.safeMint(bob,baseURI,{
                        from: bob,
                    }),
                    "Pausable: paused"
                );
            });
            it("should not not airDrop token", async function () {
                expect(await ERC721TokenInstance.paused()).to.equal(true);
                await expectRevert(
                    ERC721TokenInstance.airDrop([bob],[baseURI],{
                        from: owner,
                    }),
                    "Pausable: paused"
                );
            });
            it("should not withdraw", async function () {
                expect(await ERC721TokenInstance.paused()).to.equal(true);
                await expectRevert(
                    ERC721TokenInstance.withdraw({
                        from: owner,
                    }),
                    "Pausable: paused"
                );
            });
            it("should not add user in blacklist ", async function () {
                expect(await ERC721TokenInstance.paused()).to.equal(true);
                await expectRevert(
                    ERC721TokenInstance.addToBlackList([bob], {
                        from: owner,
                    }),
                    "Pausable: paused"
                );
            });
            it("should not remove user from blacklist", async function () {
                expect(await ERC721TokenInstance.paused()).to.equal(true);
                await expectRevert(
                    ERC721TokenInstance.removeFromBlackList([bob],{
                        from: owner,
                    }),
                    "Pausable: paused"
                );
            });
        });
    });
    describe("unpause", () => {
        describe("when other user tries to unpause contract", function () {
            it("should not unpause contract", async () => {
                expect(await ERC721TokenInstance.paused()).to.equal(true);
                await expectRevert(
                    ERC721TokenInstance.unpause({
                        from: user,
                    }),
                    "Ownable: caller is not the owner"
                );
            });
        });
        describe("when owner tries to unpause contract", function () {
            it("should unpause contract", async () => {
                expect(await ERC721TokenInstance.paused()).to.equal(true);
                const unpauseReceipt = await ERC721TokenInstance.unpause({
                    from: owner,
                });
                await expectEvent(unpauseReceipt, EventNames.Unpaused, {
                    account: owner,
                });
                expect(await ERC721TokenInstance.paused()).to.equal(false);
            });
        });
    });
    describe("tokenURI", async () => {
        describe("when users tries to get the uri", async () => {
            it("should get the uri for non-existent token", async () => {
                await expectRevert(
                    ERC721TokenInstance.tokenURI(tokenNotExist, {
                        from: user,
                    }),
                    "ERC721Token: Non-existent token."
                );
            });
            it("sholud return URI", async ()=>{
                expect(await ERC721TokenInstance.tokenURI(total - 1)).to.equal(baseURI3);
            });  
        });
    });
});

