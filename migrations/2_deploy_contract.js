const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });

const ERC721Token = artifacts.require("ERC721Token");
const NAME = process.env.NAME;
const SYMBOL = process.env.SYMBOL;
const TOKENPRICE = process.env.TOKENPRICE;
module.exports = function (deployer) {
  deployer.deploy(ERC721Token, NAME, SYMBOL, TOKENPRICE);
};
